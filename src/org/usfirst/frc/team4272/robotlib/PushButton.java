package org.usfirst.frc.team4272.robotlib;

public class PushButton {
	private boolean prev = false;
	public boolean update(boolean next) {
		boolean ret = false;
		if (next && ! prev) {
			ret = true;
		}
		prev = next;
		return ret;
	}
}
