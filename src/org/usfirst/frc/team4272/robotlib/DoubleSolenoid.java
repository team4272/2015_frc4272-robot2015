package org.usfirst.frc.team4272.robotlib;

public class DoubleSolenoid extends edu.wpi.first.wpilibj.DoubleSolenoid {
	private boolean enabled = true;
	private Value value;
	
	public DoubleSolenoid(int forwardChannel, int reverseChannel) {
		super(forwardChannel, reverseChannel);
		value = get();
	}
	public DoubleSolenoid(int moduleNumber, int forwardChannel, int reverseChannel) {
		super(moduleNumber, forwardChannel, reverseChannel);
		value = get();
	}
	
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
		if (enabled) {
			set(value);
		} else {
			set(Value.kOff);
		}
	}
	
	public void setForward(boolean forward) {
		set(forward ? Value.kForward : Value.kReverse);
	}
	
	public boolean getForward() {
		return value == Value.kForward;
	}
	
	public void set(Value value) {
		this.value = value;
		if (enabled) {
			super.set(value);
		}
	}
}
