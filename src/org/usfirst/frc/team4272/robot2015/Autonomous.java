package org.usfirst.frc.team4272.robot2015;

import edu.wpi.first.wpilibj.Timer;

public class Autonomous {
	private final Timer time = new Timer();
	private final HwRobot robot;

	public Autonomous(HwRobot robot) {
		this.robot = robot;
		time.reset();
		time.start();
		robot.rDriveE.reset();
		robot.lDriveE.reset();
	}

	public Control run(Control c) {
		if (robot.rDriveE.getDistance() > -(15.5*140*1.15)) {
			c.rDrive = c.lDrive = 0.7;
			//initially -0.7 to drive backwards
			//0 to 9787 for going backwards
			//0 to negative for forward 
			//INDY: (15.5*140*1.1)
			//PURDUE: (15.5*140*1.15) for a drop further
		} else {
			c.rDrive = c.lDrive = 0;
		}
		return c;
	}
}
