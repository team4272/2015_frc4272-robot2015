package org.usfirst.frc.team4272.robot2015;

import org.usfirst.frc.team4272.robotlib.ToggleButton;
import org.usfirst.frc.team4272.robotlib.PushButton;
import org.usfirst.frc.team4272.robotlib.Xbox360Controller.Axis;
import org.usfirst.frc.team4272.robotlib.Xbox360Controller.Button;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.Relay;

public class Teleop {
	private ToggleButton grabButton = new ToggleButton();
	private ToggleButton pushButton = new ToggleButton();
	private PushButton camButton = new PushButton();

	public Teleop() {
	}

	private static double jsScale(Joystick j) {
		double y = -j.getY();/* +:forward; -:backward */
		double z = -j.getZ();/* +:more-sensitive; -:less-sensitive */
		return Math.copySign(Math.pow(Math.abs(y), 2.0-z), y);
	}

	public Control run(Control control, HwOI oi) {
		/* Drive */
		control.lDrive = jsScale(oi.lStick);
		control.rDrive = jsScale(oi.rStick);
		/* Winch */
		control.winch = oi.xbox.getAxis(Axis.LY ) * -1;/* up is neg, down is pos */
		if (Math.abs(control.winch) < 0.1) { control.winch = 0; }

		/* left intake */
		if (oi.xbox.getButton(Button.LB)) {
			control.lIntake = Relay.Value.kReverse;
		} else if (oi.xbox.getAxis(Axis.LT) > 0.75) {
			control.lIntake = Relay.Value.kForward;
		} else {
			control.lIntake = Relay.Value.kOff;
		}

		/* right intake */
		if (oi.xbox.getButton(Button.RB)) {
			control.rIntake=(Relay.Value.kReverse);
		} else if (oi.xbox.getAxis(Axis.RT) > 0.75) {
			control.rIntake=(Relay.Value.kForward);
		} else {
			control.rIntake=(Relay.Value.kOff);
		}

		control.grab = grabButton.update(oi.xbox.getButton(Button.A));
		control.push = pushButton.update(oi.xbox.getButton(Button.B));
		
		if (camButton.update(oi.xbox.getButton(Button.RT))) {
			try {
				ProcessBuilder pb = new ProcessBuilder("/home/lvuser/tweeterbot/bin/snapPhoto");
				pb.redirectErrorStream(true);
				pb.start();
			} catch (Exception e) {};
		}

		return control;
	}
}
