package org.usfirst.frc.team4272.robot2015;

import edu.wpi.first.wpilibj.Relay;

public class Control {
	double lDrive, rDrive, winch;
	Relay.Value lIntake, rIntake;
	boolean grab, push;

	public Control() {
		lDrive = rDrive = winch = 0;
		grab = push = false;
		lIntake = rIntake = Relay.Value.kOff;
	}
}
